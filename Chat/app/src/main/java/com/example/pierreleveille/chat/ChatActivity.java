package com.example.pierreleveille.chat;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ChatActivity extends AppCompatActivity {
    ArrayList<MessageObj> listeMess;
    ArrayList<String> listeMembres;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        recupMessages();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction =fragmentManager.beginTransaction();

        for (MessageObj message : listeMess){
            MessageItemFragment fragment = MessageItemFragment.newInstance(message);

            transaction.add(R.id.fenetreChat,fragment);
        }
        transaction.commit();

        for (String membre : listeMembres){
            MembreItemFragment fragment1 = MembreItemFragment.newInstance(membre);

            transaction.add(R.id.membres,fragment1);
        }
        transaction.commit();


    }

    public ArrayList<MessageObj> recupMessages(){

        String serviceName = "read-messages";
        JSONObject obj = new JSONObject();


        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://apps-de-cours.com/web-chat/server/api/" +
                serviceName;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            Log.i("App read-messages", "réussi"); // Le retour
                            JSONObject perso = new JSONObject(response);
                            Log.i("API nom", perso.getString("nomUsager"));
                            Log.i("API message",  perso.getString("message"));
                            Log.i("API prive",  perso.getString("prive"));
                            MessageObj obj= new MessageObj(perso.getString("nomUsager"),perso.getString("message"),Boolean.parseBoolean(perso.getString("prive")));
                           listeMess.add(obj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("App", "Erreur...");
                    }
                }
        );
        queue.add(stringRequest);

        return listeMess;
    }

    public ArrayList<String> recupMembres(){

        String serviceName = "read-members";
        JSONObject obj = new JSONObject();


        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://apps-de-cours.com/web-chat/server/api/" +
                serviceName;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("App read-membres", response); // Le retour
                        listeMembres.add(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("App", "Erreur...");
                    }
                }
        );
        queue.add(stringRequest);

        return listeMembres;
    }


}

